export const addTweetAction = (post) => ({
  type: "ADD_POST",
  payload: post,
});

export const setUser = (user) => ({
  type: "SET_USER",
  payload: user,
});

export const setPosts = (post) => ({
  type: "SET_POSTS",
  payload: post,
});

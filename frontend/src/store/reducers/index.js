import { combineReducers } from "redux";
import messagesReducer from "./messagesReducer";
import postsReducer from "./postsReducer";
import usersReducer from "./usersReducer";
import userReducer from "./userReducer";
import {persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage';



const persistConfig = {
  key:'root',
  storage,
  whitelist:['user', 'users', 'posts',],
}

const rootReducer =  combineReducers({
  posts: postsReducer,
  messages: messagesReducer,
  users: usersReducer,
  user: userReducer
});


export default persistReducer(persistConfig,rootReducer);
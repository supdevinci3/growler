
const initialstate = { posts: []}

function postsReducer(state = initialstate, action) {
  switch (action.type) {
    case "ADD_POST":
      var arr = [...state.posts];
      arr.unshift(action.payload);
      return { ...state, posts: arr };
  case "SET_POSTS":
      return { posts: action.payload };
    default:
      return state;
  }
}
export default postsReducer;

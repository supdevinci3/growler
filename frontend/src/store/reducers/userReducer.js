
const initialstate = {user:null};
function userReducer(state = initialstate, action) {
  switch (action.type) {
    case "SET_USER":
      return { ...state, ...action.payload };
    default:
      return state;
  }
}
export default userReducer;

import React from "react";
import { Link } from "react-router-dom";
import ChatIcon from "../../components/icons/ChatIcon";
import Logo from "../../components/icons/Logo";
import SearchIcon from "../../components/icons/SearchIcon";
import UsersIcon from "../../components/icons/UsersIcon";
import "./SignIndex.css";

function SignIndex() {
  return (
    <div className="container">
      <div className="col1">
        <div className="logo" style={{zIndex:"0"}}>
          <Logo width={1000} height={1000} transform="translate(400,80)" />
        </div>
        <div className="info">
          <div>
            <span>
              <SearchIcon className="indexIcon" /> Suis tes intérêts.
            </span>
          </div>
          <div>
            <span>
              <UsersIcon className="indexIcon" />
             {" "} Un endroit sans fake news.
            </span>
          </div>
          <div>
            <span>
              <ChatIcon className="indexIcon" /> Rejoins la meute !
            </span>
          </div>
        </div>
      </div>
      <div style={{zIndex:"100"}} className="col2">
        <div className="menu">
          <Logo width={150} fill="white" />
          <span className="header"> 
            Informations, news et actualités
          </span>
          <span className="join">Rejoignez Growller maintenant.</span>
          <div className="buttons">
            <Link to="/signup" className="signup">
              <div className="signupItem">
                <span className="signupText">S'inscrire</span>
              </div>
            </Link>
            <Link to="/login" className="login">
              <div className="loginItem">
                <span className="loginText">S'authentifier</span>
              </div>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SignIndex;

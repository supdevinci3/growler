import React from "react";
import "./Signup.css";
import Logo from "../../components/icons/Logo";
import TextInput from "../../components/TextInput/TextInput";
import { Button } from "@material-ui/core";
import { useSelector } from "react-redux";
import axios from "axios";
import { useHistory } from 'react-router-dom';



function Signup() {
  const profile = useSelector((state) => state.user);
  const navigate = useHistory()

  return (
    <div className="signUpContainer">
      <div className="card">
        <div className="signuplogo">
          <Logo width={80} height={80} fill="white" />
        </div>
        <div className="signupHeader">
          <span>Créez votre compte</span>
        </div>
        <TextInput text="Prénom"  name="first_name"/>
        <TextInput text="Nom de famille" name="last_name"/>
        <TextInput text="Email" name="email" />
        <TextInput text="Nom d'utilisateur" name="username"/>
        <TextInput text="Date de naissance" name="dob" />
        <TextInput text="Mot de passe" name="password" isPassword/>
        <div className="acceptTerm">
          <span>
            Quand vous vous connecté, vous acceptez
            <span className="acceptTermBlue"> les conditions d'utilisation</span> et les
            <span className="acceptTermBlue"> politiques de confidentialité</span>, y compris
            l'utilisation des <span className="acceptTermBlue">cookies</span>.
          </span>
        </div>
        
        <Button onClick={
          async () => {
            console.log(profile);
            await axios.post("http://"+window.location.hostname+":8000/api/add-user/", 
            profile)
            .then((response)=>{
              navigate.push("/home")
            })
            .catch((error)=>{
              console.log(error);
            })
          }
        } size="large" fullWidth variant="contained" className="signupBtn">
          <span className="signupText">S'inscrire</span>
        </Button>
      </div>
    </div>
  );
}

export default Signup;

import { Button } from "@material-ui/core";
import React from "react";
import { Link, useHistory } from "react-router-dom";
import Logo from "../../components/icons/Logo";
import TextInput from "../../components/TextInput/TextInput";
import "./Login.css";
import axios from "axios";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";


function Login() {
  const profile = useSelector((state) => state.user);
  const dispatch = useDispatch();

  const history = useHistory();
  return (
    <div className="container">
      <div className="panel">
        <div className="panelHeader">
          <Logo width={130} fill="white" />
          <span className="panelHeaderText">S'authentifier au Growller</span>
        </div>
        <div className="inputs">
          <TextInput text="Email ou nom d'utilisateur" name="username"/>
          <TextInput isPassword text="Mot de passe" name="password"/>
        </div>
        <Button onClick={
          ()=>{
            console.log(profile);
            axios.post("http://"+window.location.hostname+":8000/api/login/", {username: profile.username, password: profile.password})
            .then((response)=>
            {
              dispatch({ type: "SET_USER", payload: response.data });
              console.log(response);
              history.push('/home')
            }).catch((error)=>{
              console.log(error);

            })

          }
        } fullWidth to="/home" className="loginBtn">
          <span className="loginText">S'authentifier</span>
        </Button>
        <div className="loginLinks">
          <a href="/">
            <span className="link">Mot de passe oublié</span>
          </a>
          <span className="point">.</span>
          <a href="/signup">
            <span className="link">S'inscrire sur Growller</span>
          </a>
        </div>
      </div>
    </div>
  );
}

export default Login;

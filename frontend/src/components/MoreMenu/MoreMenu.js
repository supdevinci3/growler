import React from "react";
import {
  AnalyticsIcon,
  DisplayIcon,
  HelpIcon,
  MomentsIcon,
  NewslettersIcon,
  SettingsIcon,
  TopicsIcon,
} from "../icons";
import "./MoreMenu.css";
import MoreMenuItem from "./MoreMenuItem/MoreMenuItem";
const MoreMenu = ({ active }) => {
  return (
    <div className={active ? "moreMenu" : "unVisible"}>
    </div>
  );
};

export default MoreMenu;

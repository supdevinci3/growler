import React, { useState } from "react";
import "./TweetBox.css";
import { Avatar, TextareaAutosize, Typography } from "@material-ui/core";
import PhotoIcon from "../../icons/PhotoIcon";
import GifIcon from "../../icons/GifIcon";
import SurveyIcon from "../../icons/SurveyIcon";
import EmojiIcon from "../../icons/EmojiIcon";
import PlanIcon from "../../icons/PlanIcon";
import { useDispatch } from "react-redux";
import { addTweetAction } from "../../../store/actions/postActions";
import axios from "axios";
import { useSelector } from "react-redux";


function TweetBox() {
  const profile = useSelector((state) => state.user);
  const [isDeleted, setDeleted] = useState(false)

  const [tweet, setTweet] = useState({
    id: Date.now(),
    userimage:
      "",
    username: "usaedd",
    displayName: profile.user?(profile.user.first_name + " "+ profile.user.last_name):"",
    text: "",
    shareImage: "",
    date: Date.now(),
  });

  const dispatch = useDispatch();
  const tweetSubmit = async (e) => {
    e.preventDefault();
    if (tweet.text.trim() === "") return;
    console.log(tweet);
    console.log(profile);
    const post = {
      vote: 0,
      text: tweet.text,
      user: {
        avatar: profile.avatar,

        user:{
          username: profile.user.username,
          first_name: profile.user.first_name,
          last_name: profile.user.last_name,


        },
        
      }
    }
    
    console.log(profile);
    /*
    */

    await axios.post("http://"+window.location.hostname+":8000/api/verify/", 
    {text:tweet.text}).then(async (response)=>{
      if(response.data >= 0.5){
        
        dispatch({ type: "ADD_POST", payload: {...post, score: (response.data*100).toFixed(0)} });
        await axios.post("http://"+window.location.hostname+":8000/api/add-yell/", 
    {
      text: tweet.text,
      username: profile.username,
      media:"",
      score: (response.data*100).toFixed(0),
      
    })
    .then((response)=>{
      setDeleted(null)
      console.log(response);
    })
    .catch((error)=>{
      console.log(error);
    })

      }

    else{
      setDeleted("Votre Growl n'a pas été publié parce que il est susceptible d'être fake.");
    }
    }).catch((error)=>{
      console.log(error);
    })

   
       
      
 

    
 
    setTweet({ ...tweet, text: "" });
  };
  return (
    <>
   
      <form className="tweetbox" onSubmit={(e) => tweetSubmit(e)}>
        <div className="tweetboxRow">
          <div className="tweetboxUserIcon">
            <Avatar src={profile.avatar} />
          </div>
          <div className="tweetbox-input-row">
          <TextareaAutosize
          minRows={3}
                        value={tweet.text}
                        onChange={(e) => setTweet({ ...tweet, text: e.target.value })}
                        className="tweetbox-input"
                        placeholder="Envie de publier un growl ?"/>
          
          </div>
        </div>
        <div className="tweetboxRow">
          <div style={{ flex: 0.1 }}></div>
          <div className="tweetboxOptions">
            <PhotoIcon className="tweetboxOptionIcon" width={22} height={22} />
            <GifIcon className="tweetboxOptionIcon" width={22} height={22} />
            <SurveyIcon className="tweetboxOptionIcon" width={22} height={22} />
            <EmojiIcon className="tweetboxOptionIcon" width={22} height={22} />
            <PlanIcon className="tweetboxOptionIcon" width={22} height={22} />
            <button type="submit" className="tweetbox-button">
              Growl
            </button>
          </div>
          </div>
          {isDeleted !== null?
        <Typography style={{color: "red"}} variant="body">
          {isDeleted}
         </Typography>:null}
      </form>
      <div className="bottomBorder"></div>
    </>
  );
}

export default TweetBox;

import React from "react";
import "./Feed.css";
import TweetBox from "./TweetBox/TweetBox";
import Post from "./Post/Post";
import HomeStars from "../icons/HomeStars";
import BottomSidebar from "../BottomSidebar/BottomSidebar";
import { useSelector, useDispatch, connect } from "react-redux";
import { Avatar } from "@material-ui/core";
import DrawerBar from "../DrawerBar/DrawerBar";
import Loading from "../Loading/Loading";
import axios from "axios"

function Feed() {
  const profile = useSelector((state) => state.user);

  React.useEffect(()=>{
    axios.get('http://'+window.location.hostname+':8000/api/yells/')
  .then((response)=>{
    console.log(response);
    dispatch({ type: "SET_POSTS", payload: response.data });
  })
  axios.post('http://'+window.location.hostname+':8000/api/login/',{username:"clement"})
  .then((response)=>{
    console.log(response);
    dispatch({ type: "SET_USER", payload: response.data });
  })
  .catch((error)=>{console.log(error);})
    
  },[])
  const { posts } = useSelector((state) => state).posts;
  const dispatch = useDispatch()
  const [isDrawerBar, setIsDrawerBar] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  setTimeout(() => {
    setLoading(false);
  }, 2000);
  return (
    <section className="feed">
      {isDrawerBar && (
        <div onClick={() => setIsDrawerBar(false)} className="drawerBarPanel" />
      )}
      <DrawerBar active={isDrawerBar} />
      <div className="feed-header">
        <div onClick={() => setIsDrawerBar(true)}>
          <Avatar src={profile.avatar} />
        </div>
        <div className="feed-headerText">
          <span>Accueil</span>
        </div>
        <div className="homeStarsCol">
          <HomeStars className="homeStars" width={22} height={22} />
        </div>
      </div>
      <TweetBox />
      {loading ? (
        <Loading />
      ) : (
        <article>
          {posts.map((post) => (
            <Post
              key={post.id}
              username={post.user.user.username}
              userimage={post.user.avatar}
              date={post.date}
              displayName={post.user.user.first_name + " " + post.user.user.last_name }
              text={post.text}
              shareImage={post.media}
              score={post.score}
              vote={post.vote}
            />
          ))}
        </article>
      )}
      <BottomSidebar />
    </section>
  );
}


export default Feed;


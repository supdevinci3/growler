import React from "react";
import "./Sidebar.css";
import SidebarItem from "./SidebarItem/SidebarItem";
import {
  HomeIcon,
  MessagesIcon,
  ListIcon,
  UserIcon,
  ExploreIcon,
  SetTweetIcon,
  NotificationsIcon,
  BookmarkIcon,
  MoreIcon,
  Logo,
} from "../icons/index";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import { Avatar } from "@material-ui/core";
import { Link, useLocation } from "react-router-dom";
import MoreMenu from "../MoreMenu/MoreMenu";
import { useSelector } from "react-redux";

function Sidebar() {
  const profile = useSelector((state) => state.user);

  const [location] = React.useState(useLocation().pathname);
  const [moreActive, setMoreActive] = React.useState(false);
  return (
    <div className="sidebar">
      <Logo width={80} className="twitter-icon" />
      <Link to="/home" style={{ textDecoration: "none" }}>
        <SidebarItem
          text="Accueil"
          Icon={HomeIcon}
          active={location === "/home" && true}
        />
      </Link>
      <Link to="/explore" style={{ textDecoration: "none" }}>
        <SidebarItem
          text="Découvrir"
          Icon={ExploreIcon}
          active={location === "/explore" && true}
        />
      </Link>
      <Link to="/Notifications" style={{ textDecoration: "none" }}>
        <SidebarItem
          text="Notifications"
          Icon={NotificationsIcon}
          active={location === "/Notifications" && true}
        />
      </Link>
      <Link to="/Messages" style={{ textDecoration: "none" }}>
        <SidebarItem
          text="Messages"
          Icon={MessagesIcon}
          active={location === "/Messages" && true}
        />
      </Link>
     
      <Link to="/Profile" style={{ textDecoration: "none" }}>
        <SidebarItem
          text="Profil"
          Icon={UserIcon}
          active={location === "/Profile" && true}
        />
      </Link>
      <div
        onClick={() => setMoreActive(!moreActive)}
        className="moreMenuButton"
      >
        <MoreMenu active={moreActive} />
        {moreActive && <div className="closeMoreMenuPanel" />}
      </div>
      <div className="tweetButton">
        <SetTweetIcon className="setTweetIcon" />
        <span>Growl</span>
      </div>
      <div className="profileCard">
        <div className="profileCardImage">
          <Avatar src={profile.avatar} />
        </div>
        <div className="profileCardNameCol">
          <div className="profileCardNameColName">
            <span>{profile.user?(profile.user.first_name + " " + profile.user.last_name):null}</span>
          </div>
          <div className="profileCardNameColuserName">
            <span>{profile.user?(profile.user.username):null}</span>
          </div>
        </div>
        <div className="profileCardIcon">
          <MoreHorizIcon />
        </div>
      </div>
    </div>
  );
}

export default Sidebar;

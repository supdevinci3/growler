import { Avatar, Button } from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  AddIcon,
  BookmarkIcon,
  DisplayIcon,
  HelpIcon,
  NewslettersIcon,
  SettingsIcon,
  UserIcon,
} from "../icons";
import MoreMenuItem from "../MoreMenu/MoreMenuItem/MoreMenuItem";
import "./DrawerBar.css";
const DrawerBar = ({ active }) => {
  const profile = useSelector((state) => state.user);
  const history = useHistory()
  return (
    <div className={`drawerBar ${active && "active"}`}>
      <div className="drawerBarHeader">
        <span>Account Info</span>
        <span>X</span>
      </div>
      <div className="draweBarScroll">
        <div className="drawerBarProfile">
          <div>
            <Avatar  src={profile.avatar}/>
            <AddIcon />
          </div>
          <span>{profile.user?(profile.user.first_name + " " + profile.user.last_name):null}</span>
          <span>@{profile.user?(profile.user.username):null}</span>
          <div>
            <span>
              <span>167</span>
              <span>Following</span>
            </span>
            <span>
              <span>167</span>
              <span>Followers</span>
            </span>
          </div>
        </div>
        <MoreMenuItem title="Profile" Icon={UserIcon} link="/Profile" />
        <MoreMenuItem title="Enregistrements" Icon={BookmarkIcon} link="/Profile" />
        <Button style={{color:"white"}} onClick={()=>{
          history.push('/')
        }}>Se déconnecter</Button>
      </div>
    </div>
  );
};

export default DrawerBar;

import React from "react";
import { setUser } from "../../store/actions/postActions";
import "./TextInput.css";
import { useDispatch } from "react-redux";


function TextInput({ text, name , isPassword}) {
  const [clicked, setClicked] = React.useState(false);
  const [inputFocus, setInputFocus] = React.useState(false);
  const [value, setValue] = React.useState("");
  const dispatch = useDispatch();
  function isValueSet() {
    if (value === "") {
      setClicked(false);
    } else {
      setClicked(true);
    }
    setInputFocus(false);
  }
  return (
    <div
      className={
        inputFocus ? "textInputRow textInputRowActive" : "textInputRow"
      }
    >
      <label
        for={text}
        className={
          clicked ? "textInputLabel textInputLabelActive" : "textInputLabel"
        }
      >
        {text}
      </label>
      <input
        type={isPassword?"password":"text"}
        className="textInput"
        id={text}
        name={value}
        onChange={(e) => {
          setValue(e.target.value);
          dispatch({ type: "SET_USER", payload: {[name]: e.target.value} });
   }}
        onFocus={() => {
          setInputFocus(true);
          setClicked(true);
        }}
        onBlur={() => isValueSet()}
      />
    </div>
  );
}

export default TextInput;

import { Avatar } from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
import { VerifiedIcon } from "../icons";
import "./ProfileCard.css";

const ProfileCard = ({ active }) => {
  const [isVisible, setIsVisible] = React.useState(false);
  const profile = useSelector((state) => state.user);

  return (
    <div
      className={
        active || isVisible ? "profileDetailCard" : "profileDetailCardActive"
      }
      onMouseEnter={() => setIsVisible(true)}
      onMouseLeave={() => setIsVisible(false)}
    >
      <div>
        <Avatar src={profile.avatar}/>
        <div>
          <span>Follow</span>
        </div>
      </div>
      <div>
        <span>{profile.user?(profile.user.first_name + " " + profile.user.last_name):null}</span>
        <VerifiedIcon />
      </div>
      <div>
        <span>{profile.user?(profile.user.username):null}</span>
      </div>
      <div>
        <span>SUP DE VINCI</span>
      </div>
      <div>
        <span>
          <span>167</span>
          <span>Following</span>
        </span>
        <span>
          <span>167</span>
          <span>Followers</span>
        </span>
      </div>
    </div>
  );
};

export default ProfileCard;

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class Profil(models.Model):
    user =  models.ForeignKey(User, on_delete=models.CASCADE)
    avatar = models.CharField(max_length=300, null=True)
    cover = models.CharField(max_length=300, null=True)
    location = models.CharField(max_length=300)
    bio = models.CharField(max_length=500, null=True)

class Yell(models.Model):
    user =  models.ForeignKey(Profil, on_delete=models.CASCADE)
    text = models.CharField(max_length=5000)
    media = models.CharField(max_length=500)
    title = models.CharField(max_length=500, null=True)
    date_of_publication = models.IntegerField(default=0)
    commentsCount = models.IntegerField(default=0)
    reyellsCount = models.IntegerField(default=0)
    likesCount= models.IntegerField(default=0)
    score = models.IntegerField(default=0)

# Create your models here.
class Comment(models.Model):
    user =  models.ForeignKey(Profil, on_delete=models.CASCADE)
    yell =  models.ForeignKey(Yell, on_delete=models.CASCADE)
    text = models.CharField(max_length=500)
    media = models.CharField(max_length=500)


class Reyell(models.Model):
    user =  models.ForeignKey(Profil, on_delete=models.CASCADE)
    yell =  models.ForeignKey(Yell, on_delete=models.CASCADE)

class Follower(models.Model):
    followed = models.ForeignKey(Profil, on_delete=models.CASCADE, related_name= "followed")
    follower = models.ForeignKey(Profil, on_delete=models.CASCADE, related_name= "follower")

class Bookmark(models.Model):
    user =  models.ForeignKey(Profil, on_delete=models.CASCADE)
    yell =  models.ForeignKey(Yell, on_delete=models.CASCADE)
from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from django.http import HttpResponse, HttpResponseForbidden
from .serializers import UserSerializer, YellSerializer, ProfilSerializer
from rest_framework.response import Response
from django.contrib.auth.models import User
import json
from .models import Profil, Yell
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
from .algo import get_prediction
# Create your views here.
#@api_view(['GET'])
#def register:

@api_view(['POST'])
def login(request):
    data = json.loads(request.body.decode('utf-8'))
    try:
        user = User.objects.get(username=data["username"])
        profile = Profil.objects.get(user=user)
        serializer = ProfilSerializer(profile, many=False)
        return Response(serializer.data)
    except Exception: 
        print(Exception)
        traceback.print_exc()
        return HttpResponseForbidden("Utilisaater non ajouté")

import traceback

@api_view(['GET'])
def yells(request):
    try:
        list_yells = Yell.objects.all()
        serializer = YellSerializer(list_yells, many=True)
        return Response(serializer.data)
    except Exception:
        traceback.print_exc()
        return HttpResponseForbidden("erreur")




@api_view(['POST'])
def register(request):
    data = json.loads(request.body.decode('utf-8'))
    try:
        new_user = User(
        first_name= data["first_name"], 
        last_name= data["last_name"],
        email= data["email"], 
        password=data["password"],
        username= data['username'])
        new_user.save()
        print("here")
        new_profil = Profil(
            user= new_user,
            #dob = data["dob"]
            )

        new_profil.save()
        return Response("Utilisateur ajouté")
    except Exception: 
        print(Exception)
        traceback.print_exc()
        return HttpResponseForbidden("Utilisaater non ajouté")

@api_view(["POST"])
def add_yell(request):
    data = json.loads(request.body.decode('utf-8'))
    try: 
        user = User.objects.get(username = data['username'])
        profile = Profil.objects.get(user = user)
        new_yell = Yell(user=profile, text=data['text'], media=data["media"], score= data["score"])
        new_yell.save()
        return Response("post ajouté")
    except:
        print(Exception)
        traceback.print_exc()
        return HttpResponseForbidden("post non ajouté")
        

@api_view(["POST"])
def verify(request):
    data = json.loads(request.body.decode('utf-8'))
    try: 
        score = get_prediction(data['text'])
        return Response(score)
    except:
        print(Exception)
        traceback.print_exc()
        return HttpResponseForbidden("post non ajouté")
    



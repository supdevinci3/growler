from keras_preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Embedding, GlobalMaxPooling1D, Flatten, Conv1D, Dropout, Activation
from keras.preprocessing.text import Tokenizer
from sklearn.utils import resample
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from nltk.corpus import stopwords
import os
import re
import string
import nltk
nltk.download('stopwords')


# Hyperparams if GPU is available
if len(tf.config.list_physical_devices('GPU')) > 0:
    # GPU
    BATCH_SIZE = 128 # Number of examples used in each iteration
    EPOCHS = 4 # Number of passes through entire dataset
    VOCAB_SIZE = 90000 # Size of vocabulary dictionary
    MAX_LEN = 1000 # Max length of news (in words)
    EMBEDDING_DIM = 40 # Dimension of word embedding vector

# Hyperparams for CPU training
else:
    # CPU
    BATCH_SIZE = 120
    EPOCHS = 4
    VOCAB_SIZE = 30000
    MAX_LEN = 500
    EMBEDDING_DIM = 40

realnews = pd.read_csv("/home/stoune/Desktop/twitter/growler/backend/output/real_news_cleaned.csv", usecols=['title','text'])
fakenews = pd.read_csv("/home/stoune/Desktop/twitter/growler/backend/output/fake_news_cleaned.csv", usecols=['title','text'])

#resample dataset
realnews = resample(realnews, replace=False, n_samples=len(fakenews), random_state=0)

LABELS = ['fake', 'true']
fakenews['label'] = 0
realnews['label'] = 1

# fusionnée les 2 dataset
df_news = pd.concat([fakenews, realnews])
# permutation des données
df_news = df_news.sample(len(df_news), random_state=4)

# préparation de donnée de training(80%), validation(20%)
L = len(df_news)
train, val = df_news[:int(L * 0.8)].reset_index(), df_news[int(L * 0.8):].reset_index()

stoplist = stopwords.words('french')

#delete stopwords
def remove_stop_words(tokens):
    return [word for word in tokens if word not in stoplist]

# Custom Tokenizer
def text_normalization(text):
    text = re.sub(u"[^A-Za-z0-9(),!?\'\`:/\-\.]", u" ", text)
    text = re.sub(u'&#8217;',u"'",text)
    text = re.sub(u'\u2019',u"'",text)
    text = re.sub(u'&#160;',u" ",text)
    text = re.sub(u'&#8211;',u"-",text)
    text = re.sub(u'\u000a',u"",text)
    text = re.sub(u'\u2014',u"-",text)
    text = re.sub(u'\n+$',u" ",text)
    text = re.sub(u'\s+',u" ",text)
    text = re.sub(u'^\s+',u"",text)
    text = re.sub(u'\s+$',u"",text)
    text = re.sub(u"\'s", u" \'s", text)
    text = re.sub(u"\'ve", u" \'ve", text)
    text = re.sub(u"n\'t", u" n\'t", text)
    text = re.sub(u"\'re", u" \'re", text)
    text = re.sub(u"\'d", u" \'d", text)
    text = re.sub(u"\'ll", u" \'ll", text)
    text = re.sub(u",", u" , ", text)
    text = re.sub(u"!", u" ! ", text)
    text = re.sub(u"\(", u" ( ", text)
    text = re.sub(u"\)", u" ) ", text)
    text = re.sub(u"\?", u" ? ", text)
    text = re.sub(u"\.{2,}",u" . ", text)
    text = re.sub(u":[^/]",u" : ", text)
    text = re.sub(u"\s{2,}",u" ", text)
    return text

def lower_token(tokens):
    return [w.lower() for w in tokens]

def tokenize(s):
    return lower_token(text_normalization(s).split())

#clean text
train['text_clean'] = train['text'].apply(lambda x: text_normalization(x))
val['text_clean'] = val['text'].apply(lambda x: text_normalization(x))
#remove stop words
train['text_clean'] = [ ' '.join(remove_stop_words(text.split())) for text in train['text_clean'] ]
val['text_clean'] = [ ' '.join(remove_stop_words(text.split())) for text in val['text_clean'] ]

text_tokenizer = Tokenizer(num_words=VOCAB_SIZE)
text_tokenizer.fit_on_texts(train['text_clean'].values)

x_train_seq = text_tokenizer.texts_to_sequences(train['text_clean'].values)
x_val_seq = text_tokenizer.texts_to_sequences(val['text_clean'].values)

x_train = sequence.pad_sequences(x_train_seq, maxlen=MAX_LEN, padding="post", value=0)
x_val = sequence.pad_sequences(x_val_seq, maxlen=MAX_LEN, padding="post", value=0)

y_train, y_val = train['label'].values, val['label'].values



# Model Parameters
NUM_FILTERS = 120
KERNEL_SIZE = 2
HIDDEN_DIMS = 200

# CNN Model
model = Sequential()

# we start off with an efficient embedding layer which maps
# our vocab indices into EMBEDDING_DIM dimensions
model.add(Embedding(VOCAB_SIZE, EMBEDDING_DIM, input_length=MAX_LEN))
model.add(Dropout(0.2))

# we add a Convolution1D, which will learn NUM_FILTERS filters
model.add(Conv1D(NUM_FILTERS,
                 KERNEL_SIZE,
                 padding='valid',
                 activation='relu',
                 strides=1))

# we use max pooling:
model.add(GlobalMaxPooling1D())

# We add a vanilla hidden layer:
model.add(Dense(HIDDEN_DIMS))
model.add(Dropout(0.1))
model.add(Activation('relu'))

# We project onto a single unit output layer, and squash it with a sigmoid:
model.add(Dense(1))
model.add(Activation('sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
model.summary()

# fit a model
model.fit(x_train, y_train,
          batch_size=BATCH_SIZE,
          epochs=EPOCHS,
          validation_split=0.1,
          verbose=2)

# Evaluate the model
score, acc = model.evaluate(x_val, y_val, batch_size=BATCH_SIZE)


# Plot confusion matrix
from sklearn.metrics import confusion_matrix

pred = (model.predict(x_val) > 0.5)*1


cnf_matrix = confusion_matrix(pred, y_val)

# Print Precision Recall F1-Score Report
from sklearn.metrics import classification_report

report = classification_report(pred, y_val, target_names=LABELS)

fig, ax = plt.subplots(1, 1, figsize=(8, 8))

cax = ax.matshow(cnf_matrix, cmap='cool')


for (i, j), z in np.ndenumerate(cnf_matrix):
    ax.text(j, i, z, ha='center', va='center')

def get_prediction(text):
    # Preprocessing
    text_np_array = text_tokenizer.texts_to_sequences([text])
    text_np_array = sequence.pad_sequences(text_np_array, maxlen=MAX_LEN, padding="post", value=0)
    # Prediction
    score = model.predict(text_np_array)[0][0]
    #print('test ', model.predict(text_np_array))
    prediction = LABELS[(model.predict(text_np_array) > 0.5 )[0][0]]
    print('\nPREDICTION:', prediction, '\nSCORE: ', score)
    return score



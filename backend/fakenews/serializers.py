from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Yell, Profil

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name',"id")

class ProfilSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profil
        fields = ('user',"id", 'avatar')
        depth=2

class YellSerializer(serializers.ModelSerializer):
    class Meta:
        model = Yell
        fields = ('text', 'media','user','date_of_publication', "title", "score","id" )
        depth=2
